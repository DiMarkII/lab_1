#ifndef _TREE_H_
#define _TREE_H_

#include <iostream>
#include <string>
#include <vector>

template <typename T_key, typename T_value>
class tree
{
private:
    enum nodeColor
    {
        BLACK,
        RED
    };
    struct Node;
    inline static Node *NIL, sentinel;
    struct Node
    {
        Node *left;
        Node *right;
        Node *parent;
        nodeColor color;
        T_key key;
        T_value value;
        ~Node() {if (this != NIL){
                 if (left && left != NIL) delete left;
                 if (right && right != NIL) delete right;}}
    };
    Node *root;

    void rotateLeft(Node *x);
    void rotateRight(Node *x);
    void insertFixup(Node *x);
    Node *_insertNode(T_key key, T_value value);
    void deleteFixup(Node *x);
    Node *findNode(T_key key);
    void deleteNode(Node *z);
    void initialise();
    void _print(Node *x);
    void _get_keys(std::vector<T_key> &v, Node *x);
    void _get_values(std::vector<T_value> &v, Node *x);
public:
    void insert(T_key key, T_value value);
    void remove(T_key key);
    T_value *find(T_key key);
    tree() : root{NIL} { initialise(); }
    ~tree() {if (root != NIL) delete root;}
    void clear() {if (root != NIL) {delete root; root = NIL;}}
    void print();
    std::vector<T_key> get_keys();
    std::vector<T_value> get_values();
};

template <typename T_key, typename T_value>
void tree<T_key, T_value>::_get_keys(std::vector<T_key> &v, Node *x)
{
    if (x && x != NIL)
    {
        _get_keys(v, x->left);
        v.push_back(x->key);
        _get_keys(v, x->right);
    }
}

template <typename T_key, typename T_value>
void tree<T_key, T_value>::_get_values(std::vector<T_value> &v, Node *x)
{
    if (x && x != NIL)
    {
        _get_values(v, x->left);
        v.push_back(x->value);
        _get_values(v, x->right);
    }
}

template <typename T_key, typename T_value>
std::vector<T_key> tree<T_key, T_value>::get_keys()
{
    std::vector<T_key> result;
    _get_keys(result, root);
    return result;
}

template <typename T_key, typename T_value>
std::vector<T_value> tree<T_key, T_value>::get_values()
{
    std::vector<T_value> result;
    _get_values(result, root);
    return result;
}

template <typename T_key, typename T_value>
void tree<T_key, T_value>::print()
{
    _print(root);
}

template <typename T_key, typename T_value>
void tree<T_key, T_value>::_print(Node *x)
{
    if (x && x != NIL)
    {
        _print(x->left);
        std::cout << "key: " << x->key
                  << "; value: " << x->value
                  << "; color: " << (x->color == BLACK ? "black" : "red")
                  << "; left: " << (x->left && x->left != NIL ? std::to_string(x->left->key) : "-")
                  << "; right: " << (x->right && x->right != NIL ? std::to_string(x->right->key) : "-")
                  << "; parent: " << (x != root && x->parent ? std::to_string(x->parent->key) : "this is root")
                  << '\n';
        _print(x->right);
    }
}

template <typename T_key, typename T_value>
void tree<T_key, T_value>::initialise()
{
    NIL = &sentinel;
    sentinel.left = NIL;
    sentinel.right = NIL;
    sentinel.parent = nullptr;
    sentinel.color = BLACK;
}

template <typename T_key, typename T_value>
void tree<T_key, T_value>::insert(T_key key, T_value value)
{
    _insertNode(key, value);
}

template <typename T_key, typename T_value>
void tree<T_key, T_value>::remove(T_key key)
{
    Node *temp = findNode(key);
    if (temp)
        deleteNode(temp);
}

template <typename T_key, typename T_value>
T_value *tree<T_key, T_value>::find(T_key key)
{
    Node *temp = findNode(key);
    if (temp)
        return &temp->value;
    else
        return nullptr;
}

template <typename T_key, typename T_value>
void tree<T_key, T_value>::rotateLeft(Node *x)
{
    Node *y = x->right;

    x->right = y->left;
    if (y->left != NIL)
        y->left->parent = x;

    if (y != NIL)
        y->parent = x->parent;
    if (x->parent)
    {
        if (x == x->parent->left)
            x->parent->left = y;
        else
            x->parent->right = y;
    }
    else
    {
        root = y;
    }

    y->left = x;
    if (x != NIL)
        x->parent = y;
}

template <typename T_key, typename T_value>
void tree<T_key, T_value>::rotateRight(Node *x)
{
    Node *y = x->left;

    x->left = y->right;
    if (y->right != NIL)
        y->right->parent = x;

    if (y != NIL)
        y->parent = x->parent;
    if (x->parent)
    {
        if (x == x->parent->right)
            x->parent->right = y;
        else
            x->parent->left = y;
    }
    else
    {
        root = y;
    }

    y->right = x;
    if (x != NIL)
        x->parent = y;
}

template <typename T_key, typename T_value>
void tree<T_key, T_value>::insertFixup(Node *x)
{
    while (x != root && x->parent->color == RED)
    {
        if (x->parent == x->parent->parent->left)
        {
            Node *y = x->parent->parent->right;
            if (y->color == RED)
            {
                x->parent->color = BLACK;
                y->color = BLACK;
                x->parent->parent->color = RED;
                x = x->parent->parent;
            }
            else
            {
                if (x == x->parent->right)
                {
                    x = x->parent;
                    rotateLeft(x);
                }
                x->parent->color = BLACK;
                x->parent->parent->color = RED;
                rotateRight(x->parent->parent);
            }
        }
        else
        {
            Node *y = x->parent->parent->left;
            if (y->color == RED)
            {
                x->parent->color = BLACK;
                y->color = BLACK;
                x->parent->parent->color = RED;
                x = x->parent->parent;
            }
            else
            {
                if (x == x->parent->left)
                {
                    x = x->parent;
                    rotateRight(x);
                }
                x->parent->color = BLACK;
                x->parent->parent->color = RED;
                rotateLeft(x->parent->parent);
            }
        }
    }
    root->color = BLACK;
}

template <typename T_key, typename T_value>
typename tree<T_key, T_value>::Node *tree<T_key, T_value>::_insertNode(T_key key, T_value value)
{
    Node *current, *parent, *x;

    current = root;
    parent = 0;
    while (current && current != NIL)
    {
        if (key == current->key)
            return current;
        parent = current;
        current = key < current->key ? current->left : current->right;
    }

    x = new Node;
    x->key = key;
    x->value = value;
    x->parent = parent;
    x->left = NIL;
    x->right = NIL;
    x->color = RED;

    if (parent)
    {
        if (key < parent->key)
            parent->left = x;
        else
            parent->right = x;
    }
    else
        root = x;

    insertFixup(x);
    return x;
}

template <typename T_key, typename T_value>
void tree<T_key, T_value>::deleteFixup(Node *x)
{
    while (x != root && x->color == BLACK)
    {
        if (x == x->parent->left)
        {
            Node *w = x->parent->right;
            if (w->color == RED)
            {
                w->color = BLACK;
                x->parent->color = RED;
                rotateLeft(x->parent);
                w = x->parent->right;
            }
            if (w->left->color == BLACK && w->right->color == BLACK)
            {
                w->color = RED;
                x = x->parent;
            }
            else
            {
                if (w->right->color == BLACK)
                {
                    w->left->color = BLACK;
                    w->color = RED;
                    rotateRight(w);
                    w = x->parent->right;
                }
                w->color = x->parent->color;
                x->parent->color = BLACK;
                w->right->color = BLACK;
                rotateLeft(x->parent);
                x = root;
            }
        }
        else
        {
            Node *w = x->parent->left;
            if (w->color == RED)
            {
                w->color = BLACK;
                x->parent->color = RED;
                rotateRight(x->parent);
                w = x->parent->left;
            }
            if (w->right->color == BLACK && w->left->color == BLACK)
            {
                w->color = RED;
                x = x->parent;
            }
            else
            {
                if (w->left->color == BLACK)
                {
                    w->right->color = BLACK;
                    w->color = RED;
                    rotateLeft(w);
                    w = x->parent->left;
                }
                w->color = x->parent->color;
                x->parent->color = BLACK;
                w->left->color = BLACK;
                rotateRight(x->parent);
                x = root;
            }
        }
    }
    x->color = BLACK;
}

template <typename T_key, typename T_value>
void tree<T_key, T_value>::deleteNode(Node *z)
{
    Node *x, *y;

    if (!z || z == NIL)
        return;

    if (z->left == NIL || z->right == NIL)
        y = z;
    else
    {
        y = z->right;
        while (y->left != NIL)
            y = y->left;
    }

    if (y->left != NIL)
        x = y->left;
    else
        x = y->right;

    x->parent = y->parent;
    if (y->parent)
        if (y == y->parent->left)
            y->parent->left = x;
        else
            y->parent->right = x;
    else
        root = x;

    if (y != z)
    {
        z->key = y->key;
        z->value = y->value;
    }

    if (y->color == BLACK)
        deleteFixup(x);
    y->left = nullptr;
    y->right = nullptr;
    delete y;
}

template <typename T_key, typename T_value>
typename tree<T_key, T_value>::Node *tree<T_key, T_value>::findNode(T_key key)
{
    Node *current = root;
    while (current && current != NIL)
        if (key == current->key)
            return current;
        else
            current = key < current->key ? current->left : current->right;
    return 0;
}

#endif