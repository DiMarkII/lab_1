#include <iostream>
#include <cassert>

#include "tree.h"

int main()
{
    tree<int, int> tr;
    std::cout << "Tree:\n";
    for (int i = 1; i <= 10; ++i)
        tr.insert(i, i * i);
    tr.print();
    std::vector<int> v1, v2;
    v1 = tr.get_keys();
    v2 = tr.get_values();
    std::cout << "Keys:\n";
    for (const auto &x : v1)
        std::cout << x << ' ';
    std::cout << '\n';
    std::cout << "Values:\n";
    for (const auto &x : v2)
        std::cout << x << ' ';
    std::cout << '\n';
    tr.remove(3);
    tr.remove(8);
    tr.remove(4);
    std::cout << "Tree after erasing 3, 8, 4:\n";
    tr.print();
    v1 = tr.get_keys();
    v2 = tr.get_values();
    std::cout << "Keys:\n";
    for (const auto &x : v1)
        std::cout << x << ' ';
    std::cout << '\n';
    std::cout << "Values:\n";
    for (const auto &x : v2)
        std::cout << x << ' ';
    std::cout << '\n';
    std::cout << "Tree after erasing all:\n";
    tr.clear();
    tr.print();
    v1 = tr.get_keys();
    v2 = tr.get_values();
    std::cout << "Keys:\n";
    for (const auto &x : v1)
        std::cout << x << ' ';
    std::cout << '\n';
    std::cout << "Values:\n";
    for (const auto &x : v2)
        std::cout << x << ' ';
    std::cout << '\n';
    return 0;
}