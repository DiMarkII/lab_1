#include "CppUnitTest.h"
#include "tree.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace tests
{
    TEST_CLASS(tests){
    public :
        TEST_METHOD(insert_in_empty_tree)
        {
            tree<int, int> tr;
            tr.insert(1, 10);
            std::vector<int> keys{1}, values{10};
            Assert::IsTrue(tr.get_keys() == keys && tr.get_values() == values);
        }
        TEST_METHOD(remove_element)
        {
            tree<int, int> tr;
            tr.insert(1, 1);
            tr.insert(2, 4);
            tr.insert(3, 9);
            tr.remove(2);
            std::vector<int> keys{1, 3}, values{1, 9};
            Assert::IsTrue(tr.get_keys() == keys && tr.get_values() == values);
        }
        TEST_METHOD(test_find_function)
        {
            tree<int, int> tre;
            tr.insert(1, 1);
            tr.insert(2, 4);
            tr.insert(3, 9);
            int *temp = tr.find(1);
            Assert::IsTrue(temp && *temp == 1);
            temp = tr.find(5);
            Assert::IsTrue(temp == nullptr);
        }
        TEST_METHOD(test_get_keys)
        {
            tree<int, int> tr;
            tr.insert(1, 1);
            tr.insert(2, 4);
            tr.insert(3, 9);
            std::vector<int> keys_tr = tr.get_keys();
            std::vector<int> keys{1, 2, 3};
            Assert::IsTrue(keys_tr == keys);
        }
        TEST_METHOD(test_get_values)
        {
            tree<int, int> tr;
            tr.insert(1, 1);
            tr.insert(2, 4);
            tr.insert(3, 9);
            std::vector<int> values_tr = tr.get_values();
            std::vector<int> values{1, 4, 9};
            Assert::IsTrue(values_tr == values);
        }
    };
}